package ru.goloshchapov.tm.api.repository;

import ru.goloshchapov.tm.api.IRepository;
import ru.goloshchapov.tm.model.Task;

import java.util.Comparator;
import java.util.List;

public interface ITaskRepository extends IRepository<Task> {

    List<Task> findAll(Comparator<Task> comparator);

    List<Task> findAllStarted(Comparator<Task> comparator);

    List<Task> findAllCompleted(Comparator<Task> comparator);

    Task findOneByName(String name);

    boolean isAbsentByName(String name);

    Task removeOneByName(String name);

    Task startTaskById(String id);

    Task startTaskByIndex(Integer index);

    Task startTaskByName(String name);

    Task finishTaskById(String id);

    Task finishTaskByIndex(Integer index);

    Task finishTaskByName(String name);

    List<Task> findAllByProjectId(String projectId);

    Task bindToProjectById (String taskId, String projectId);

    Task unbindFromProjectById(String taskId);

    List<Task> removeAllByProjectId(String projectId);

}
