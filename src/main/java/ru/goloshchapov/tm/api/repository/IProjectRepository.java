package ru.goloshchapov.tm.api.repository;

import ru.goloshchapov.tm.api.IRepository;
import ru.goloshchapov.tm.model.Project;

import java.util.Comparator;
import java.util.List;

public interface IProjectRepository extends IRepository<Project> {

    List<Project> findAll(Comparator<Project> comparator);

    List<Project> findAllStarted(Comparator<Project> comparator);

    List<Project> findAllCompleted(Comparator<Project> comparator);

    Project findOneByName(String name);

    boolean isAbsentByName(String name);

    String getIdByName(String name);

    Project removeOneByName(String name);

    Project startProjectById(String id);

    Project startProjectByIndex(Integer index);

    Project startProjectByName(String name);

    Project finishProjectById(String id);

    Project finishProjectByIndex(Integer index);

    Project finishProjectByName(String name);

}
