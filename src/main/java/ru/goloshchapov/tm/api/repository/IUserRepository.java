package ru.goloshchapov.tm.api.repository;

import ru.goloshchapov.tm.api.IRepository;
import ru.goloshchapov.tm.model.User;

public interface IUserRepository extends IRepository<User> {

    User findUserByLogin(String login);

    User findUserByEmail(String email);

    boolean isLoginExists(String login);

    boolean isEmailExists(String email);

    User removeUser(User user);

    User removeUserByLogin(String login);

}
