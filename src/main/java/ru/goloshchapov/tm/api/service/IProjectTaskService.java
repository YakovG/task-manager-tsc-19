package ru.goloshchapov.tm.api.service;

import ru.goloshchapov.tm.model.Project;
import ru.goloshchapov.tm.model.Task;

import java.util.List;

public interface IProjectTaskService {

    Project addProject(String projectName, String projectDescription);

    Task addTask(String taskName, String taskDescription);

    void clearProjects();

    List<Project> findAllProjects();

    List<Task> findAllByProjectId(String projectId);

    boolean isEmptyProjectById(String projectId);

    List<Task> findAllByProjectName(String projectName);

    boolean isEmptyProjectByName(String projectName);

    List<Task> findAllByProjectIndex(Integer projectIndex);

    boolean isEmptyProjectByIndex(Integer projectIndex);

    Task bindToProjectById(String taskId, String projectId);

    Task unbindFromProjectById(String taskId, String projectId);

    List<Task> removeAllByProjectId(String projectId);

    List<Task> removeAllByProjectName(String projectName);

    List<Task> removeAllByProjectIndex(Integer projectIndex);

    Project removeProjectById(String projectId);

    Project removeProjectByName(String projectName);

    Project removeProjectByIndex(Integer projectIndex);

    void createTestData();

}
