package ru.goloshchapov.tm.api.service;

import ru.goloshchapov.tm.api.IService;
import ru.goloshchapov.tm.model.Task;

import java.util.Comparator;
import java.util.List;

public interface ITaskService extends IService<Task> {

    List<Task> findAll(Comparator<Task> comparator);

    List<Task> findAllStarted(Comparator<Task> comparator);

    List<Task> findAllCompleted(Comparator<Task> comparator);

    Task add (String name, String description);

    List<Task> sortedBy(String sortCheck);

    Task findOneByName(String name);

    Task removeOneByName(String name);

    Task updateOneById(String id, String name, String description);

    Task updateOneByIndex(Integer index, String name, String description);

    Task startTaskById(String id);

    Task startTaskByIndex(Integer index);

    Task startTaskByName(String name);

    Task finishTaskById(String id);

    Task finishTaskByIndex(Integer index);

    Task finishTaskByName(String name);

    Task changeTaskStatusById(String id, String statusChange);

    Task changeTaskStatusByName(String name, String statusChange);

    Task changeTaskStatusByIndex(int index, String statusChange);

}
