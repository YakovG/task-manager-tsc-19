package ru.goloshchapov.tm.command.project;

import ru.goloshchapov.tm.exception.entity.ProjectNotFoundException;
import ru.goloshchapov.tm.model.Project;
import ru.goloshchapov.tm.util.TerminalUtil;

public final class ProjectByIndexFinishCommand extends AbstractProjectCommand{

    public static final String NAME = "project-finish-by-index";

    public static final String DESCRIPTION ="Finish project by index";

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return NAME;
    }

    @Override
    public String description() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[FINISH PROJECT]");
        System.out.println("ENTER INDEX:");
        final int index = TerminalUtil.nextNumber() -1;
        final Project project = serviceLocator.getProjectService().finishProjectByIndex(index);
        if (project == null) throw new ProjectNotFoundException();
    }
}
