package ru.goloshchapov.tm.command.project;

import ru.goloshchapov.tm.exception.entity.ProjectNotFoundException;
import ru.goloshchapov.tm.model.Project;
import ru.goloshchapov.tm.util.TerminalUtil;

public final class ProjectByNameFinishCommand extends AbstractProjectCommand{

    public static final String NAME = "project-finish-by-name";

    public static final String DESCRIPTION ="Finish project by name";

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return NAME;
    }

    @Override
    public String description() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[FINISH PROJECT]");
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        final Project project = serviceLocator.getProjectService().finishProjectByName(name);
        if (project == null) throw new ProjectNotFoundException();
    }
}
