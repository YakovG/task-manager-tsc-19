package ru.goloshchapov.tm.command.task;

import ru.goloshchapov.tm.enumerated.Status;
import ru.goloshchapov.tm.exception.entity.TaskNotUpdatedException;
import ru.goloshchapov.tm.model.Task;
import ru.goloshchapov.tm.util.TerminalUtil;

import java.util.Arrays;

public final class TaskByIndexChangeStatusCommand extends AbstractTaskCommand{

    public static final String NAME = "task-change-status-by-index";

    public static final String DESCRIPTION = "Change task status by index";

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return NAME;
    }

    @Override
    public String description() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[CHANGE TASK STATUS]");
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        System.out.println("ENTER STATUS:");
        final Status[] statuses = Status.values();
        System.out.println(Arrays.toString(statuses));
        final String statusChange = TerminalUtil.nextLine();
        final Task task = serviceLocator.getTaskService().changeTaskStatusByName(name, statusChange);
        if (task == null) throw new TaskNotUpdatedException();
    }
}
