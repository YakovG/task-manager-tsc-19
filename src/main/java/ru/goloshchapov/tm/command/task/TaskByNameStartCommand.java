package ru.goloshchapov.tm.command.task;

import ru.goloshchapov.tm.exception.entity.TaskNotFoundException;
import ru.goloshchapov.tm.model.Task;
import ru.goloshchapov.tm.util.TerminalUtil;

public final class TaskByNameStartCommand extends AbstractTaskCommand{

    public static final String NAME = "task-start-by-name";

    public static final String DESCRIPTION = "Start task by name";

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return NAME;
    }

    @Override
    public String description() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[START TASK]");
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        final Task task = serviceLocator.getTaskService().startTaskByName(name);
        if (task == null) throw new TaskNotFoundException();
    }
}
