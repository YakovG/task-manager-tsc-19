package ru.goloshchapov.tm.command.auth;

import ru.goloshchapov.tm.command.AbstractCommand;
import ru.goloshchapov.tm.util.TerminalUtil;

public final class LoginCommand extends AbstractCommand {

    public static final String NAME = "login";

    public static final String DESCRIPTION = "Login to program";

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return NAME;
    }

    @Override
    public String description() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[LOGIN]");
        System.out.println("[ENTER LOGIN]");
        final String login = TerminalUtil.nextLine();
        System.out.println("[ENTER PASSWORD]");
        final String password = TerminalUtil.nextLine();
        serviceLocator.getAuthService().login(login,password);
    }
}
