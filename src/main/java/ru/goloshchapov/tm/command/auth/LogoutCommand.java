package ru.goloshchapov.tm.command.auth;

import ru.goloshchapov.tm.command.AbstractCommand;

public final class LogoutCommand extends AbstractCommand {

    public static final String NAME = "logout";

    public static final String DESCRIPTION = "Logout from program";

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return NAME;
    }

    @Override
    public String description() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[LOGOUT]");
        serviceLocator.getAuthService().logout();
    }
}
