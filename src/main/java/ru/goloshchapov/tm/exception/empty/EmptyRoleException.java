package ru.goloshchapov.tm.exception.empty;

import ru.goloshchapov.tm.exception.AbstractException;

public class EmptyRoleException extends AbstractException {

    public EmptyRoleException() { super("Error! Role is empty..."); }

}
