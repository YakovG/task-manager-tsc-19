package ru.goloshchapov.tm.enumerated;

import ru.goloshchapov.tm.comparator.*;

import java.util.Comparator;

public enum Sort {

    CREATED("Sort by created", ComparatorByCreated.getInstance()),
    NAME("Sort by name", ComparatorByName.getInstance()),
    STATUS("Sort by status", ComparatorByStatus.getInstance()),
    DATE_START("Sort by start date", ComparatorByDateStart.getInstance()),
    DATE_FINISH("Sort by finish date", ComparatorByDateFinish.getInstance());

    private final String displayName;

    private final Comparator comparator;

    Sort(String displayName, Comparator comparator) {
        this.displayName = displayName;
        this.comparator = comparator;
    }

    public String getDisplayName() {
        return displayName;
    }

    public Comparator getComparator() {
        return comparator;
    }

}
