package ru.goloshchapov.tm.repository;

import ru.goloshchapov.tm.api.repository.ITaskRepository;
import ru.goloshchapov.tm.enumerated.Status;
import ru.goloshchapov.tm.model.Task;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

public final class TaskRepository extends AbstractRepository<Task> implements ITaskRepository {

    @Override
    public List<Task> findAll(final Comparator<Task> comparator) {
        List<Task> tasks = new ArrayList<>(list);
        tasks.sort(comparator);
        return tasks;
    }

    @Override
    public List<Task> findAllStarted(final Comparator<Task> comparator) {
        List<Task> tasks = new ArrayList<>();
        for (final Task task:list) {
            if (task.getStatus() != Status.NOT_STARTED) tasks.add(task);
        }
        if (tasks.size() == 0) return null;
        tasks.sort(comparator);
        return tasks;
    }

    @Override
    public List<Task> findAllCompleted(final Comparator<Task> comparator) {
        List<Task> tasks = new ArrayList<>();
        for (final Task task:list) {
            if (task.getStatus() == Status.COMPLETE) tasks.add(task);
        }
        if (tasks.size() == 0) return null;
        tasks.sort(comparator);
        return tasks;
    }

    @Override
    public Task findOneByName(final String name) {
        for (final Task task:list) {
            if (name.equals(task.getName())) return task;
        }
        return null;
    }

    @Override
    public boolean isAbsentByName(final String name) {
        return findOneByName(name) == null;
    }

    @Override
    public Task removeOneByName(final String name) {
        final Task task = findOneByName(name);
        if (task == null) return null;
        list.remove(task);
        return task;
    }

    @Override
    public Task startTaskById(final String id) {
        final Task task = findOneById(id);
        if (task == null) return null;
        task.setStatus(Status.IN_PROGRESS);
        task.setDateStart(new Date());
        return task;
    }

    @Override
    public Task startTaskByIndex(final Integer index) {
        final Task task = findOneByIndex(index);
        if (task == null) return null;
        task.setStatus(Status.IN_PROGRESS);
        task.setDateStart(new Date());
        return task;
    }

    @Override
    public Task startTaskByName(final String name) {
        final Task task = findOneByName(name);
        if (task == null) return null;
        task.setStatus(Status.IN_PROGRESS);
        task.setDateStart(new Date());
        return task;
    }

    @Override
    public Task finishTaskById(final String id) {
        final Task task = findOneById(id);
        if (task == null) return null;
        task.setStatus(Status.COMPLETE);
        task.setDateFinish(new Date());
        return task;
    }

    @Override
    public Task finishTaskByIndex(final Integer index) {
        final Task task = findOneByIndex(index);
        if (task == null) return null;
        task.setStatus(Status.COMPLETE);
        task.setDateFinish(new Date());
        return task;
    }

    @Override
    public Task finishTaskByName(final String name) {
        final Task task = findOneByName(name);
        if (task == null) return null;
        task.setStatus(Status.COMPLETE);
        task.setDateFinish(new Date());
        return task;
    }

    @Override
    public List<Task> findAllByProjectId(final String projectId) {
        final List<Task> tasks = new ArrayList<>();
        for (final Task task: list) {
            if (projectId.equals(task.getProjectId())) tasks.add(task);
        }
        return tasks;
    }

    @Override
    public Task bindToProjectById(final String taskId, final String projectId) {
        final Task task = findOneById(taskId);
        if (task == null) return null;
        task.setProjectId(projectId);
        return task;
    }

    @Override
    public Task unbindFromProjectById(final String taskId) {
        final Task task = findOneById(taskId);
        if (task == null) return null;
        task.setProjectId(null);
        return task;
    }

    @Override
    public List<Task> removeAllByProjectId(final String projectId) {
        final List<Task> tasks = findAllByProjectId(projectId);
        if (tasks == null) return null;
        for (final Task task: tasks) {
            remove(task);
        }
        return tasks;
    }

}
