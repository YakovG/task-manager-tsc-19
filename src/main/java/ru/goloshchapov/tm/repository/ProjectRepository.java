package ru.goloshchapov.tm.repository;

import ru.goloshchapov.tm.api.repository.IProjectRepository;
import ru.goloshchapov.tm.enumerated.Status;
import ru.goloshchapov.tm.model.Project;

import java.util.*;

public final class ProjectRepository extends AbstractRepository<Project> implements IProjectRepository {

    @Override
    public List<Project> findAll(final Comparator<Project> comparator) {
        List<Project> projects = new ArrayList<>(list);
        projects.sort(comparator);
        return projects;
    }

    @Override
    public List<Project> findAllStarted(final Comparator<Project> comparator) {
        List<Project> projects = new ArrayList<>();
        for (final Project project:list) {
            if (project.getStatus() != Status.NOT_STARTED) projects.add(project);
        }
        if (projects.size() == 0) return null;
        projects.sort(comparator);
        return projects;
    }

    @Override
    public List<Project> findAllCompleted(final Comparator<Project> comparator) {
        List<Project> projects = new ArrayList<>();
        for (final Project project:list) {
            if (project.getStatus() == Status.COMPLETE) projects.add(project);
        }
        if (projects.size() == 0) return null;
        projects.sort(comparator);
        return projects;
    }

    @Override
    public Project findOneByName(final String name) {
        for (final Project project:list) {
            if (name.equals(project.getName())) return project;
        }
        return null;
    }

    @Override
    public boolean isAbsentByName(final String name) {
        return findOneByName(name) == null;
    }

    @Override
    public String getIdByName(final String name) {
        return findOneByName(name).getId();
    }

    @Override
    public Project removeOneByName(final String name) {
        final Project project = findOneByName(name);
        if (project == null) return null;
        list.remove(project);
        return project;
    }

    @Override
    public Project startProjectById(final String id) {
        final Project project = findOneById(id);
        if (project == null) return null;
        project.setStatus(Status.IN_PROGRESS);
        project.setDateStart(new Date());
        return project;
    }

    @Override
    public Project startProjectByIndex(final Integer index) {
        final Project project = findOneByIndex(index);
        if (project == null) return null;
        project.setStatus(Status.IN_PROGRESS);
        project.setDateStart(new Date());
        return project;
    }

    @Override
    public Project startProjectByName(final String name) {
        final Project project = findOneByName(name);
        if (project == null) return null;
        project.setStatus(Status.IN_PROGRESS);
        project.setDateStart(new Date());
        return project;
    }

    @Override
    public Project finishProjectById(final String id) {
        final Project project = findOneById(id);
        if (project == null) return null;
        project.setStatus(Status.COMPLETE);
        project.setDateFinish(new Date());
        return project;
    }

    @Override
    public Project finishProjectByIndex(final Integer index) {
        final Project project = findOneByIndex(index);
        if (project == null) return null;
        project.setStatus(Status.COMPLETE);
        project.setDateFinish(new Date());
        return project;
    }

    @Override
    public Project finishProjectByName(final String name) {
        final Project project = findOneByName(name);
        if (project == null) return null;
        project.setStatus(Status.COMPLETE);
        project.setDateFinish(new Date());
        return project;
    }

}
