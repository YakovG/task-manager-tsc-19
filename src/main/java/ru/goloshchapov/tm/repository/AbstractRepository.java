package ru.goloshchapov.tm.repository;

import ru.goloshchapov.tm.api.IRepository;
import ru.goloshchapov.tm.model.AbstractEntity;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public abstract class AbstractRepository<E extends AbstractEntity> implements IRepository<E> {

    protected final List<E> list = new ArrayList<>();

    @Override
    public void addAll(final Collection<E> collection) {
        if (collection == null) return;
        list.addAll(collection);
    }

    @Override
    public E add(final E entity) {
        list.add(entity);
        return entity;
    }

    @Override
    public List<E> findAll() { return list; }

    @Override
    public E findOneById(final String id) {
        for (final E entity:list) {
            if (id.equals(entity.getId())) return entity;
        }
        return null;
    }

    @Override
    public E findOneByIndex(final Integer index) {return list.get(index);}

    @Override
    public boolean isAbsentById(final String id) {
        return findOneById(id) == null;
    }

    @Override
    public boolean isAbsentByIndex(final Integer index) {
        return findOneByIndex(index) == null;
    }

    @Override
    public String getIdByIndex(final Integer index) {
        return findOneByIndex(index).getId();
    }

    @Override
    public int size() {
        return list.size();
    }

    @Override
    public void clear() { list.clear(); }

    @Override
    public void remove(final E entity) { list.remove(entity); }

    @Override
    public E removeOneById(final String id) {
        final E entity = findOneById(id);
        if (entity == null) return null;
        list.remove(entity);
        return entity;
    }

    @Override
    public E removeOneByIndex(final Integer index) {
        final E entity = findOneByIndex(index);
        if (entity == null) return null;
        list.remove(entity);
        return entity;
    }
}
